import React, {useState} from 'react';
import Email from './components/Email';
import Password from './components/Password';
import Button from './components/Button';



const App = () => {
  const [email,setEmail] = useState('no email provided');
  const handleChangeEmail = event => setEmail(event.target.value);
  const [password,setPassword] = useState('no password provided');
  const handleChangePassword = event => setPassword(event.target.value);


  return  (
    <div>
    <Email headline={email} onChangeHeadline={handleChangeEmail} />
    <Password headline={password} onChangeHeadline={handleChangePassword} />
    <Button password={password} email={email} />
    </div>)
};

export default App;