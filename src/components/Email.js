import React from "react";

const Email = ({ headline, onChangeHeadline }) => (
    <div>
      <h1>{headline}</h1>

      <input
        type="text"
        value={headline}
        onChange={onChangeHeadline}
      />
    </div>
)

export default Email